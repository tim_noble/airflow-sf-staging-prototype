import os, logging, pysftp
from local_config import LocalConfigParser
from file_dir_manager import FileDirManager
from sftp_operation import SftpOperation

class ResultGetter:

	def __init__(self):

		self.local_config = LocalConfigParser('settings.ini')
		
		# get files
		self.fdm = FileDirManager()
		
	def do_sftp(self):
	
		# sftp the file
		my_sftp = SftpOperation()
		my_sftp.get_directory(self.local_config.getValue('Sftp','host'),self.local_config.getValue('Sftp','encdir'),self.fdm.get_enc_dir())
		
	def run(self):
		self.do_sftp()		

if __name__ == "__main__":
	cm = ResultGetter()
	cm.run()