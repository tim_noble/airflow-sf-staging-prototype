import os, logging, pysftp
from local_config import LocalConfigParser
from file_dir_manager import FileDirManager
from sftp_operation import SftpOperation

class FileTidier:

	def __init__(self):

		self.local_config = LocalConfigParser('settings.ini')
		
		# get files
		self.fdm = FileDirManager()
	
	"""
		Empties local directories
	"""
	def tidy_local(self):
	
		dirs = [self.fdm.get_result_dir(),self.fdm.get_enc_dir()]
		
		for dir in dirs:

			for file in os.listdir(dir):
				
				file_path = os.path.join(dir, file)
				
				if os.path.isfile(file_path):
					os.unlink(file_path)
	
	"""
		Empties remote sftp directories
	"""
	def tidy_remote(self):
		
		my_sftp = SftpOperation()
		my_sftp.empty_directory(self.local_config.getValue('Sftp','host'), self.local_config.getValue('Sftp','encdir'))
		
		
	def run(self):
		self.tidy_local()
		self.tidy_remote()
		

if __name__ == "__main__":
	cm = FileTidier()
	cm.run()