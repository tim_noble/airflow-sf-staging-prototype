import os
from sqlalchemy import create_engine, sql
from csv_encryptor import CsvEncryptor
from io import StringIO
from local_config import LocalConfigParser
import pandas as pd
from file_dir_manager import FileDirManager

class ImportProcess:

	def run(self):
	
	
		fdm = FileDirManager()
		enc_file_name = os.path.join(fdm.get_enc_dir(),'picklist_labels.enc')
		enc = CsvEncryptor()
		sio = StringIO(enc.decrypt(enc_file_name))
		df = pd.read_csv(sio)
		local_config = LocalConfigParser('settings.ini')
		# df.to_csv(os.path.join(fdm.get_result_dir(),'temp.csv'), index=False)
		
		#db connection properties
		db_host = local_config.getValue('SfPostgres','host')
		db_database = local_config.getValue('SfPostgres','database')
		db_user = local_config.getValue('SfPostgres','user')
		db_password = local_config.getValue('SfPostgres','password')

		# build connection string from the local_config properties
		conn_str = 'postgresql://%(user)s:%(password)s@%(host)s/%(database)s' % {'user' : db_user, 'password' : db_password,'host' : db_host, 'database' : db_database}
				
		# return a new SqlAlchemyEngine
		engine = create_engine(conn_str, echo=True)

		# import the dataframe to the database
		df.to_sql('picklists',engine, if_exists='replace', index=False)
			
		# prepare a statement
		statement = sql.text("ALTER TABLE picklists ADD PRIMARY KEY (option_id)")
		
		# execute the statement
		engine.execute(statement)

if __name__ == "__main__":
	cm = ImportProcess()
	cm.run()