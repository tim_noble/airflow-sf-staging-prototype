import configparser, os

"""
	Uses configparser to get values from our local ini
"""
class LocalConfigParser:
	
	"""
		Reads the local ini file on initialization
	"""
	def __init__(self, fileName):
		# new config parser
		self.Config = configparser.ConfigParser()
		# read the local .ini
		self.Config.read(os.path.join(os.path.dirname(os.path.realpath(__file__)),fileName))
	
	"""
		Pulls the chosen section into a dictionary and returns
	"""
	def ConfigSectionMap(self, section):
		dict1 = {}
		options = self.Config.options(section)
		for option in options:
			try:
				dict1[option] = self.Config.get(section, option)
				if dict1[option] == -1:
					DebugPrint("skip: %s" % option)
			except:
				print("exception on %s!" % option)
				dict1[option] = None
		return dict1
	
	"""
		Get the value of the option from the section
	"""
	def getValue(self, section, option) :
		dict = self.ConfigSectionMap(section)
		return dict[option]
	