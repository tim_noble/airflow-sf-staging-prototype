import pysodium, os

class CsvEncryptor:

	def encrypt(self,csvio,filePath):
	
		# get the keys
		public, secret = self.getKeys()
		
		# generate
		nonce = os.urandom(pysodium.crypto_box_NONCEBYTES)
		
		# encrypt the msg
		c = pysodium.crypto_box(csvio.getvalue().encode("utf-8"), nonce, public, secret)

		# create a new nonceFile
		nonceFile = self.getNonceFilePath(filePath)
		
		# write to the nonce file
		with open(nonceFile,'wb') as f:
			f.write(nonce)

		# write the cyphertext to the encrypted file
		encFile = self.getFileNamePortion(filePath) + '.enc'
		
		with open(encFile,'wb') as f:
			f.write(c)
		
		# return paths of both enc and nonce
		return encFile, nonceFile
	
	"""
		Example of generating keypairs
	"""
	def generateKeys():
		
		pkA, skA = pysodium.crypto_box_keypair()
		pkB, skB = pysodium.crypto_box_keypair()
	
	def getKeys(self):
		
		# read the public key
		with open('/home/airflow/keys/sodium/public', 'rb') as f:
			public = f.read()
		
		# read the private key
		with open('/home/airflow/keys/sodium/secret', 'rb') as f:
			secret = f.read()
			
		return public, secret
		
	def getFileNamePortion(self, filePath):
		
		# get the basename from the filePath
		split = os.path.splitext(filePath)
		
		return split[0]
	
	def getNonceFilePath(self, filePath):
			
		# get the realPath from the file
		realPath = os.path.dirname(os.path.realpath(filePath))
		
		# nonce is basename of file + '.non' extension
		nonceName = self.getFileNamePortion(filePath) + '.non'
		
		return os.path.join(realPath,nonceName)
		
	def decrypt(self,filePath):
	
		# get the keys
		public, secret = self.getKeys()

		nonceFile = self.getNonceFilePath(filePath)
		
		# open the nonce
		with open(nonceFile,'rb') as f:
			nonce = f.read()
		
		# remove the nonceFile
		os.remove(nonceFile)

		# open the file
		with open(filePath,'rb') as f:
			c = f.read()
		
		# remove the encrypted file
		os.remove(filePath)
		
		# decrypt the contents of the file
		o = pysodium.crypto_box_open(c, nonce, public, secret)

		# return the decrypted text
		return o.decode("utf-8")