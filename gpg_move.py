import os, logging, pysftp
from local_config import LocalConfigParser
from file_dir_manager import FileDirManager
from sftp_operation import SftpOperation

class GpgMover:

	def __init__(self):

		self.local_config = LocalConfigParser('settings.ini')
		
		# get files
		fdm = FileDirManager()
		self.gpg_source_file = os.path.join(fdm.get_result_dir(),self.local_config.getValue('Gpg','resultfilename'))
		#self.gpg_target_file = os.path.join(self.local_config.getValue('Dir','move'),os.path.basename(self.gpg_source_file))
		
	def os_move(self):
		os.rename(self.gpg_source_file,self.gpg_target_file)
		
	def do_sftp(self):
	
		# sftp the file
		my_sftp = SftpOperation()
		my_sftp.put(self.local_config.getValue('Sftp','host'),self.gpg_source_file,self.local_config.getValue('Sftp','movedir'))
		
	def run(self):
		self.do_sftp()		

if __name__ == "__main__":
	cm = GpgMover()
	cm.run()