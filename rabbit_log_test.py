from logbook import Logger
from logbook.queues import MessageQueueHandler

mq_handler = MessageQueueHandler('amqp://synergy:35-four-FISH-22@timrabbit//', queue='airflow_log')

with mq_handler.applicationbound():
	my_logger = Logger()	
	my_logger.debug("Testing, sir")

# mq_handler.close()