import pysftp
from local_config import LocalConfigParser

class SftpOperation:

	def __init__(self):

		self.local_config = LocalConfigParser('settings.ini')
		self.private_key = '~/.ssh/id_rsa'
		
	def put(self, host, from_file, to_dir):
		
		# connect to remote server
		with pysftp.Connection(host = host, private_key = self.private_key) as my_sftp:

			#cd to target dir
			with my_sftp.cd(to_dir):

				#put result file
				my_sftp.put(from_file, preserve_mtime=False)
				
	def get_directory(self, host, from_dir, to_dir):

		# connect to remote server
		with pysftp.Connection(host = host, private_key = self.private_key) as my_sftp:

			#cd to target dir
			with my_sftp.cd(from_dir):
			
				print(from_dir, to_dir)

				#put result file
				my_sftp.get_d(from_dir, to_dir, preserve_mtime=False)
		
				
	def empty_directory(self, host, empty_dir):
	
		# connect to remote server
		with pysftp.Connection(host = host, private_key = self.private_key) as my_sftp:

			# note the lambda function to remove files found by walktree
			my_sftp.walktree(empty_dir, lambda f : my_sftp.remove(f), None, None)		