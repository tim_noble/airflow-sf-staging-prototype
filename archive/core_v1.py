from io import StringIO
from local_config import LocalConfigParser
from file_dir_manager import FileDirManager
from collections import OrderedDict
from pprint import pprint
from csv_encryptor import CsvEncryptor
from datetime import datetime
from enum import Enum
from ECConfig import ECConfig
from sqlalchemy import create_engine, sql
import base64, os, requests, sys, tempfile, uuid, re
import ijson.backends.yajl2_cffi as y_ijson
import ijson
import pandas as pd
import gnupg
import dpath.util
import pysodium
import time
import logging
import logging.handlers

"""
Some reusable functions
"""
class CustomUtils:

	@staticmethod
	def current_milli_time():
		return int(round(time.time() * 1000))
	
	@staticmethod
	def milli_diff(end, start):
		return (end-start)/1000
	
class PicklistUtils:
		
	def __init__(self, dbh) :
		
		self.dbh = dbh
	
	def getLabel(self, optionId):
	
		# prepare a statement
		statement = sql.text("SELECT label FROM picklists WHERE option_id = :option_id")
		
		# parameters for the above statement
		data = {'option_id':optionId}
		
		# execute the above statement
		result = self.dbh.engine.execute(statement, data)
		
		# try to get a row from the result proxy
		row = result.fetchone()
		
		# free up the connection back to the pool
		result.close()
		
		if row is not None:
			return row['label']

""" Base class for SfApi """
class SfApiClient:
	
	def __init__(self):
		
		self.local_config = LocalConfigParser('settings.ini')
		self.ec_config = ECConfig(self.local_config.getValue('SFAPI','targetinstance'))
		self.username = self.ec_config.getSection('UrlQuery')['user']
		self.password = self.ec_config.getSection('UrlQuery')['password']

"""
	Takes care of HTTP interaction with the SF OData API
"""
class SfOdataClient(SfApiClient):

	# Constructor.  Setting up properties for later use.
	def __init__(self):
	
		# call parent's __init__
		super().__init__()
		
		#set up base properties
		self.base_url = self.ec_config.getSection('UrlQuery')['baseurl']
		self.auth = base64.b64encode((self.username + ':' + self.password).encode('ascii'))
		self.custom_headers = {'Authorization': 'Basic ' + self.auth.decode('ascii'), 'Content-Type': 'application/json'}
		self.request_timeout = self.local_config.getValue('OData','timeout')
		self.download_path = None
		self.file_dir_man = FileDirManager()
		self.request_count = 0
		self.total_download_seconds = 0
		self.total_ijson_seconds = 0
		self.total_next_seconds = 0
		self.total_download_bytes = 0
		
	# get base params for query
	def getBaseParams(self):
		base_params = {}
		base_params['$format'] = self.local_config.getValue('OData','format')
		
		# any top param?
		top = int(self.local_config.getValue('OData','top'))
		
		if top > 0:
			base_params['$top'] = top
			
		# any filter?
		filter = self.local_config.getValue('OData','filter')
		
		if filter != '':
			base_params['$filter'] = filter
		
		return base_params
	
	# Main method.  Handles the communication with the API and the conversion of JSON to Python objects
	def process(self, query, next):

		# session should give a performance boost
		with requests.Session() as s:
			
			if next is not None:
				url = next
				params = None
			else:
				url = self.base_url + query.odata_base_entity
				params = {**self.getBaseParams(),**query.odata_query_params}
				
				if query.add_key is not None:
					# add key to select
					if query.add_key['api'] not in params['$select']:
						select = params['$select'].split(',')
						select.append(query.add_key['api'])
						params['$select'] = ",".join(select)
			
			# increment total request count
			self.request_count = self.request_count + 1
			
			# >> log
			logging.getLogger(__name__).info('Request #%(request_count)d' % {'request_count' : self.request_count})
			
			# start time of request
			request_start_time = CustomUtils.current_milli_time()
			
			# >> log
			logging.getLogger(__name__).info('Download started at %(request_start_time)d' % {'request_start_time' : request_start_time})
			
			# make request to url
			r = s.get(url, params=params, headers=self.custom_headers, stream=True, timeout=float(self.request_timeout))
						
			""" if the request was successful, stream the response to a file on disk """
			if r.status_code == 200:

				# open a secure tempfile
				fd, path = tempfile.mkstemp()
				
				# open a tempfile but don't delete it immediately when closed as ijson will need to reopen it
				with os.fdopen(fd, 'wb') as f:
					
					#set the temp file's name as the download path
					self.download_path = path

					download_bytes = 0
					
					for chunk in r.iter_content(1024):
						
						# if you wanted to write the string
						# f.write(chunk.decode('utf-8', 'ignore'))
						
						# writing the raw bytes here
						f.write(chunk)
						
						download_bytes = download_bytes + len(chunk)
							
					# add size of download to total download size for run
					self.total_download_bytes = self.total_download_bytes + download_bytes
				
				#end time of download
				request_end_time = CustomUtils.current_milli_time()
				
				# >> log
				logging.getLogger(__name__).info('Download ended at %(request_end_time)d' % {'request_end_time' : request_end_time})
				
				# >> log
				logging.getLogger(__name__).info('Total download size %(download_bytes)d bytes' % {'download_bytes' : download_bytes})
				
				# difference between end and start time to show total number of seconds taken
				request_download_seconds = CustomUtils.milli_diff(request_end_time,request_start_time)			
				
				# >> log
				logging.getLogger(__name__).info('Total download time %(download_time)0.3f seconds' % {'download_time' : request_download_seconds})
				
				# add time taken to total for run
				self.total_download_seconds = self.total_download_seconds + request_download_seconds
				
				# ijson start time
				ijson_start_time = CustomUtils.current_milli_time()
				
				# >> log
				logging.getLogger(__name__).info('IJson started at %(ijson_start_time)d' % {'ijson_start_time' : ijson_start_time})
				
				#parse the json into objects
				objects = ijson.items(open(self.download_path,'r'), 'd.results.item')
				
				#store the objects in a var
				records = (o for o in objects)
				
				# ijson start time
				ijson_end_time = CustomUtils.current_milli_time()
				
				# >> log
				logging.getLogger(__name__).info('IJson ended at %(ijson_end_time)d' % {'ijson_end_time' : ijson_end_time})
				
				# total ijson time this run
				ijson_time = CustomUtils.milli_diff(ijson_end_time, ijson_start_time)
				
				# >> log
				logging.getLogger(__name__).info('Total IJson time %(ijson_time)0.3f' % {'ijson_time' : ijson_time})
				
				# add total ijson this run to overall total
				self.total_ijson_seconds = self.total_ijson_seconds + ijson_time
				
				# does the json contain a next url
				self.next = self.getNextUrl()
				
				# pull the records into a dictionary
				response = {
					'data' : records,
					'next' : next,
					'download_path' : self.download_path
				}
				
				# now delete the temp file
				os.unlink(self.download_path)
				
				#return our response dictionary
				return response
				
			else:
				error_path = self.store_error_text(r.text)
				raise RuntimeError('Server returned a %(code)s status code. Report in %(path)s' % {'code' : str(r.status_code), 'path' : error_path})

	# extracts the next token from the JSON
	def getNextUrl(self):
		
		ret = None
		next_element = 'd.__next'
		
		# next start time
		next_start_time = CustomUtils.current_milli_time()
		
		logging.getLogger(__name__).info('Get next started at %(next_start_time)d' % {'next_start_time' : next_start_time})

		with open(self.download_path, 'rb') as f:

			parser = y_ijson.parse(f)

			for prefix, event, value in parser:
				if (prefix, event) == (next_element, 'string'):
					ret = value
					break

		# next end time
		next_end_time = CustomUtils.current_milli_time()
		
		logging.getLogger(__name__).info('Get next ended at %(next_end_time)d' % {'next_end_time' : next_end_time})
		
		# difference between end and start time to show total number of seconds taken
		next_time = CustomUtils.milli_diff(next_end_time, next_start_time)			
		
		logging.getLogger(__name__).info('Total get next time %(next_time)0.3f seconds' % {'next_time' : next_time})
		
		# add next time for this run to total for overall totals
		self.total_next_seconds = self.total_next_seconds + next_time
		
		return ret
		
		"""
		try:
			f = open(self.download_path, 'r')
		except Exception as ex:
			pass
		else:
			logging.getLogger(__name__).info('Start')
			parser = ijson.parse(f)

			for prefix, event, value in parser:
				if (prefix, event) == (next_element, 'string'):
					ret = value
					break
			f.close()
			logging.getLogger(__name__).info('
		finally:
			# next end time
			next_end_time = CustomUtils.current_milli_time()
			
			logging.getLogger(__name__).info('Get next ended at %(next_end_time)d' % {'next_end_time' : next_end_time})
			
			# difference between end and start time to show total number of seconds taken
			next_time = CustomUtils.milli_diff(next_end_time, next_start_time)			
			
			logging.getLogger(__name__).info('Total get next time %(next_time)0.3f seconds' % {'next_time' : next_time})
			
			# add next time for this run to total for overall totals
			self.total_next_seconds = self.total_next_seconds + next_time
			
			return ret
	"""
	
	# encrypts error message and stores in on the server. returns filename for logging
	def store_error_text(self, error_text):
		
		#uuid for error file
		error_uuid = str(uuid.uuid4())
		
		# error path
		error_path = os.path.join(self.file_dir_man.get_error_dir(),error_uuid)
		
		with open(error_path, 'w') as f:
		
			f.write(error_text)
			
			"""
			# gpg using keys in current user's home
			gpg = gnupg.GPG(gnupghome='~/.gnupg')
			
			# recipient for whom error messages should be encrypted
			recipient = self.local_config.getValue('Error','errorrecipient')
			
			# encrypt the error text
			encrypted_error_text = gpg.encrypt(error_text, recipient)
			
			# write the error text to the file
			f.write(str(encrypted_error_text))
			"""
			
		return error_path
	
""" Base class for query """
class SfQuery:
	def __init__(self, kwargs):
		self.odata_base_entity = kwargs['odata_base_entity']
		self.type = kwargs['type']
		self.odata_query_params = kwargs['odata_query_params']
		self.add_key = kwargs['add_key'] if 'add_key' in kwargs else None

""" Runs the query and handles api pagination """		
class SfQueryRunner:

	def __init__(self, query, responder):
		
		self.query = query
		self.responder = responder
		self.total_transform_seconds = 0	
		
		# set up logging
		#self.log_utils = LogUtils(query.type)
		#self.log_utils.set_query_type(query.type)
	
	def walkRecords(self, next):
	
		try:
			
			# pass the result of the query to the responder's process method	
			transform_seconds = self.responder.process(self.client.process(self.query, next))
			self.total_transform_seconds = self.total_transform_seconds + transform_seconds
			
			# are there any additional records?
			if self.client.next is not None:
				self.walkRecords(self.client.next)
			else:
				
				# remove the temp file
				#os.remove(ceClient.download_path)
			
				# write the dataframe to csv in StringIO
				csvio = StringIO()
				df = pd.DataFrame(self.responder.dataList)
				
				# drop duplicates on key column
				if self.query.add_key is not None:
					df.drop_duplicates(subset=self.query.add_key['df'], keep='first', inplace=True)
				
				# convert data frame to csv
				df.to_csv(csvio, index=False)
				
				# log totals
				logging.getLogger(__name__).info('Overall download time %(total_time)0.3f seconds' % {'total_time' : self.client.total_download_seconds})
				logging.getLogger(__name__).info('Overall ijson time %(total_time)0.3f seconds' % {'total_time' : self.client.total_ijson_seconds})
				logging.getLogger(__name__).info('Overall next time %(total_time)0.3f seconds' % {'total_time' : self.client.total_next_seconds})
				logging.getLogger(__name__).info('Overall download size %(total_size)0.3f bytes' % {'total_size' : self.client.total_download_bytes})
				logging.getLogger(__name__).info('Overall transform time %(total_time)0.3f seconds' % {'total_time' : self.total_transform_seconds})
				
				# encrypt the csv
				c = CsvEncryptor()
				c.encrypt(csvio, self.responder.enc_csv_file_name)
				
				# prune the log files
				# self.log_utils.prune()

		except:
			raise
				
	def run(self):

		self.client = SfOdataClient()
		self.walkRecords(None)

class LogUtils:

	def __init__(self,):		
		
		# self.ch = None
		# self.fh = None
		# keep 3 months of logs
		#self.max_age = (7 * 86400) * 12
		self.max_age = 86400
		
		## load log dir from FileDirManager
		fdm = FileDirManager()
		self.log_dir = fdm.get_log_dir()
		
	def set_query_type(self, query_type):
	
		self.query_type = query_type
	
		# stream handler for logging to console
		if self.ch is not None:
			self.rootLogger.removeHandler(self.ch)
			
		if self.fh is not None:
			self.rootLogger.removeHandler(self.fh)
		
		self.ch = logging.StreamHandler()
		self.ch.setFormatter(self.logFormatter)
		self.rootLogger.addHandler(self.ch)
		
		# file handler for logging to console
		self.fh = logging.FileHandler(self.get_log_file_name(self.query_type))
		self.fh.setFormatter(self.logFormatter)
		self.rootLogger.addHandler(self.fh)
	
		self.query_type = query_type

	def get_log_file_name(self, query_type):

		log_file_pattern = '%(query_type)s_%(time)d.log' % {'query_type' : query_type, 'time' : CustomUtils.current_milli_time()}
		log_path = os.path.join(self.log_dir,log_file_pattern)
		
		return log_path

	def prune(self):
		
		now = time.time()

		for f in os.listdir(self.log_dir):
		
			log_file = os.path.join(self.log_dir,f)
			
			if os.path.isfile(log_file):

				if os.stat(log_file).st_mtime < now - self.max_age:

					os.remove(log_file)
		
""" Returns query runner classes in response to config params """		
class QueryRunnerFactory:

	def make(self, qc, kwargs):
	
		# qc.init_log(kwargs['type'])
	
		responder = QueryResponderFactory.make(qc.dbh, kwargs)
		
		return SfQueryRunner(SfQuery(kwargs),responder)

class QueryResponderFactory:
	
	@staticmethod
	def make(dbh, kwargs) :
		
		if (kwargs['type'] == 'email'):
			return SfEmailQueryResponder(dbh, kwargs)
		elif (kwargs['type'] == 'home_address'):
			return SfHomeAddressQueryResponder(dbh, kwargs)
		elif (kwargs['type'] == 'personal_info'):
			return SfPersonalInfoQueryResponder(dbh, kwargs)
		elif (kwargs['type'] == 'phone'):
			return SfPhoneQueryResponder(dbh, kwargs)
		elif (kwargs['type'] == 'emp_info'):
			return SfEmploymentInfoQueryResponder(dbh, kwargs)
		elif (kwargs['type'] == 'job_info'):
			return SfJobInfoQueryResponder(dbh, kwargs)
		elif (kwargs['type'] == 'location_address'):
			return SfLocationAddressQueryResponder(dbh, kwargs)
		elif (kwargs['type'] == 'location'):
			return SfLocationQueryResponder(dbh, kwargs)
		elif (kwargs['type'] == 'company'):
			return SfCompanyQueryResponder(dbh, kwargs)
		elif (kwargs['type'] == 'position'):
			return SfPositionQueryResponder(dbh, kwargs)
		elif (kwargs['type'] == 'comp'):
			return SfCompQueryResponder(dbh, kwargs)
		elif (kwargs['type'] == 'comp_two'):
			return SfComp2QueryResponder(dbh, kwargs)
		elif (kwargs['type'] == 'aap_group'):
			return SfAapGroupQueryResponder(dbh, kwargs)
		elif (kwargs['type'] == 'pay_grade'):
			return SfPayGradeQueryResponder(dbh, kwargs)
		elif (kwargs['type'] == 'user'):
			return SfUserQueryResponder(dbh, kwargs)
		elif (kwargs['type'] == 'person'):
			return SfPersonQueryResponder(dbh, kwargs)
		elif (kwargs['type'] == 'division'):
			return SfDivisionQueryResponder(dbh, kwargs)
		elif (kwargs['type'] == 'cost_center'):
			return SfCostCenterQueryResponder(dbh, kwargs)
		elif (kwargs['type'] == 'gl2'):
			return SfGl2QueryResponder(dbh, kwargs)
		elif (kwargs['type'] == 'gl4'):
			return SfGl4QueryResponder(dbh, kwargs)
		elif (kwargs['type'] == 'gl5'):
			return SfGl5QueryResponder(dbh, kwargs)
		elif (kwargs['type'] == 'gl6'):
			return SfGl6QueryResponder(dbh, kwargs)
		elif (kwargs['type'] == 'gl7'):
			return SfGl7QueryResponder(dbh, kwargs)
		elif (kwargs['type'] == 'gl8'):
			return SfGl8QueryResponder(dbh, kwargs)
		elif (kwargs['type'] == 'gl9'):
			return SfGl9QueryResponder(dbh, kwargs)
		elif (kwargs['type'] == 'gl10'):
			return SfGl10QueryResponder(dbh, kwargs)
			
""" Base class for QueryResponders """
class SfQueryResponder:
	
	def	__init__(self, dbh, kwargs):

		self.picklist_utils = PicklistUtils(dbh)
		self.numResults = 0
		self.dataList = []
		self.total_transform_seconds = 0
		self.type = kwargs['type']
		
		self.transform_start_time = 0
		self.transform_end_time = 0
		
		## use FileDirManager to set up enc dir
		fdm = FileDirManager()
		self.enc_csv_directory = fdm.get_enc_dir()
		self.enc_csv_file_name = os.path.join(self.enc_csv_directory,kwargs['enc_csv_name'])

	def getData(self, response, rec_base, dict_path):
		
		# extract data from response
		records = response['data']
		
		# log start
		self.log_start()
		
		o_dict = OrderedDict()

		for v in rec_base.values():
			o_dict[v.csv_label] = None


		# loop through the results
		for record in records:
		
			res_dict = OrderedDict()

			# merge the personIdExternal if it's at the top level of record, rather than down in the results
			if 'personIdExternal' in record:
				res_dict = OrderedDict({**{'personIdExternal' : record['personIdExternal']}, **o_dict})

			#logging.getLogger(__name__).info(record['personIdExternal'])

			# pull the data out of the json
			de = DictExtractor()
			
			# extract the end result using the dict_path
			result = record if dict_path is None else de.extract(record, dict_path.copy())

			# some records don't have the entity we're looking for
			if result is not None and len(result) > 0:
			
				# if we've got a list, get the first entry, otherwise keep res as is
				result = result[0] if isinstance(result, list) else result

				# loop through the list of data items that we want to get from the res
				for i in rec_base.values():
				
					value = i.extract_value(result)
				
					# extract the value to the result dict
					if (isinstance(i, PicklistLookupItem)):
						value = self.picklist_utils.getLabel(value)
					
					res_dict[i.csv_label] = value
			
			self.dataList.append(res_dict)
		
		# log end
		self.log_end()
		
		# return total processing time so overall totals can be calculated
		return self.total_transform_seconds

	def log_start(self):
		self.transform_start_time = CustomUtils.current_milli_time()
		logging.getLogger(__name__).info('Transform started at %(transform_start_time)d' % {'transform_start_time' : self.transform_start_time})
	
	def log_end(self):
		transform_end_time = CustomUtils.current_milli_time()
		logging.getLogger(__name__).info('Transform ended at %(transform_end_time)d' % {'transform_end_time' : transform_end_time})
		self.total_transform_seconds = CustomUtils.milli_diff(transform_end_time, self.transform_start_time)
		logging.getLogger(__name__).info('Total transform time %(total_transform_seconds)0.3f seconds' % {'total_transform_seconds' : self.total_transform_seconds})

class SfMvQueryResponder(SfQueryResponder):
	
	def __init__(self, dbh, kwargs):
		super().__init__(dbh, kwargs)
		self.inter_dict = OrderedDict()
		
	def proceed(self, line, line_arg):
		
		if line_arg is None:
			return True
		else:
			if line_arg.key in line:
				if line[line_arg.key] == line_arg.value:
					return True
			else:
				raise ValueError(line_arg.key + ' not in line')
		
		return False

	def getData(self, response, dict_path, item_lookup_key, item_collection):
		
		# extract data from response
		records = response['data']
		
		# log start
		self.log_start()
	
		# loop through the results
		for record in records:

			self.inter_dict = OrderedDict()
		
			# merge the personIdExternal if it's at the top level of record, rather than down in the results
			if 'personIdExternal' in record:
				self.inter_dict['personIdExternal'] = record['personIdExternal']
				
			# item_collection to inter_dict
			for item in item_collection.items.values():
				self.inter_dict[item.csv_label] = item.value

			# pull the data out of the json
			res = dpath.util.get(record, dict_path)

			# some records don't have the entity we're looking for
			if len(res) > 0 is not None:

				# res returns as list
				for line in res:
				
					if (item_lookup_key in line) :
						i = item_collection.get(line[item_lookup_key])
						if (i is not None):
							self.inter_dict[i.csv_label] = i.extract_value(line)
					else:
						raise ValueError("Unable to find %(item_lookup_key)s in line" % {'item_lookup_key' : item_lookup_key})
			
			# inter_dict to dataList
			self.dataList.append(self.inter_dict)
			
		# log end
		self.log_end()
		
		# return total processing time so overall totals can be calculated
		return self.total_transform_seconds

class ValueExtractorFactory:

	@staticmethod
	def make(data_type, value):
		if data_type == DataItemDataType.DATE :
			return DateValueExtractor(value)
		else:
			return StringValueExtractor(value)

class ValueExtractor:
	
	def __init__(self, value):
		self.value = value

class DictExtractor:

	def extract(self, data, chunks):
		
		if len(chunks) == 1:
			try:
				return data[0][chunks[0]] if isinstance(data, list) else data[chunks[0]]
			except TypeError as ex:
				return None

		try:
			result = data[0][chunks[0]] if isinstance(data, list) else data[chunks[0]]
			
			# remove the first element from chunks list
			chunks.pop(0)
			
			return self.extract(result, chunks)
		except IndexError:
			return None

class StringValueExtractor(ValueExtractor):
	
	def extract(self):
		return self.value
		
class DateValueExtractor(ValueExtractor):

	def extract(self):

		if self.value is not None:
			# slice the timestamp
			value = self.value[6:19]
			if value.isdigit():
				timestamp = int(value)/1000
				return datetime.utcfromtimestamp(timestamp).strftime('%Y-%m-%d %H:%M:%S')

class DataItemPropertyType(Enum):
	FLAT = 1
	PICKLIST = 2
		
class DataItemDataType(Enum):
	STRING = 1
	DATE = 2
	
class DataItemCollection:

	def __init__(self, init_items = None):
		self.items = OrderedDict() if init_items is None else init_items

	def put(self, data_item):
		self.items[data_item.label] = data_item
		
	def get(self, label):
		
		if label in self.items:
			return self.items[label]
	
class DataItem:

	def __init__(self, data_type, label, csv_label, value):
		self.data_type = data_type
		self.label = label
		self.csv_label = csv_label
		self.value = value
	
class FlatDataItem(DataItem):

	def __init__(self, data_type, label, csv_label, relative_path, value):

		super().__init__(data_type, label, csv_label, value)
		
		self.relative_path = relative_path

	def extract_value(self, line):
		
		#get the value we want from the json in the line
		target_val = dpath.util.get(line, self.relative_path)
		
		# new value extractor for the target_val
		extractor = ValueExtractorFactory.make(self.data_type, target_val)

		return extractor.extract()
	
class PicklistDataItem(DataItem):

	def __init__(self, data_type, label, csv_label, parent_path, required_locale, value):

		super().__init__(data_type, label, csv_label, value)
		self.parent_path = parent_path
		self.required_locale = required_locale

	def extract_value(self, line):
		
		# do we have any picklist options in the parent_path
		parent_nav = dpath.util.get(line, self.parent_path)
		
		if (parent_nav is not None and 'picklistLabels' in parent_nav):
		
			labels = dpath.util.get(parent_nav, 'picklistLabels')
		
			if 'results' in labels:
			
				target_val = None
				
				for val in labels['results']:
					if val['locale'] == self.required_locale:
						target_val = val['label']
						break
				
				if target_val is not None:
					# new value extractor for the target_val
					extractor = ValueExtractorFactory.make(self.data_type, target_val)
					return extractor.extract()
				else:
					raise ValueError("Unable to find matching label for locale " + self.required_locale)

class PicklistLookupItem(FlatDataItem) : pass
					
""" Class for processing results from email queries """
class SfEmailQueryResponder(SfMvQueryResponder):

	def	__init__(self, dbh, kwargs):
		super().__init__(dbh, kwargs)
		
	def process(self, response):
	
		item_collection = DataItemCollection(
			OrderedDict(
				{
					'14685' : FlatDataItem(DataItemDataType.STRING, 'emailAddress','workEmail','emailAddress',None)
				}
			)
		)

		return self.getData(response, '/personNav/emailNav/results', 'emailType', item_collection)
		
""" Class for processing results from email queries """
class SfPhoneQueryResponder(SfMvQueryResponder):

	def	__init__(self, dbh, kwargs):
		super().__init__(dbh, kwargs)
		
	def process(self, response):
	
		item_collection = DataItemCollection(
			OrderedDict(
				{
					'19506' : FlatDataItem(DataItemDataType.STRING, 'phoneNumber','home','phoneNumber',None),
					'28061' : FlatDataItem(DataItemDataType.STRING, 'phoneNumber','workVoiceMail','phoneNumber',None),
					'28142' : FlatDataItem(DataItemDataType.STRING, 'phoneNumber','workExtension','phoneNumber',None),
					'28141' : FlatDataItem(DataItemDataType.STRING, 'phoneNumber','personalCell','phoneNumber',None),
					'19502' : FlatDataItem(DataItemDataType.STRING, 'phoneNumber','work','phoneNumber',None)
				}
			)
		)

		return self.getData(response, '/personNav/phoneNav/results', 'phoneType', item_collection)

""" Class for processing results from home adddress queries """
class SfHomeAddressQueryResponder(SfQueryResponder):

	def	__init__(self, dbh, kwargs):
		super().__init__(dbh, kwargs)
	
	def process(self, response):
		
		rec_dict = OrderedDict(
			{
				'address1' : FlatDataItem(DataItemDataType.STRING, 'address1','homeAddress1','address1',None),
				'address2' : FlatDataItem(DataItemDataType.STRING, 'address2','homeAddress2','address2',None),
				'address3' : FlatDataItem(DataItemDataType.STRING, 'address3','homeAddress3','address3',None),
				'city' : FlatDataItem(DataItemDataType.STRING, 'city','homeCity','city',None),
				'county' : FlatDataItem(DataItemDataType.STRING, 'county','homeCounty','county',None),
				'state' : FlatDataItem(DataItemDataType.STRING, 'state', 'homeState','state', None),
				'country' : FlatDataItem(DataItemDataType.STRING, 'country','homeCountry','country',None),
				'startDate' : FlatDataItem(DataItemDataType.DATE, 'startDate','homeStartDate','startDate',None),
				'endDate' : FlatDataItem(DataItemDataType.DATE, 'endDate','homeEndDate','endDate',None),
			}
		)

		return self.getData(response, rec_dict, ['personNav','homeAddressNavDEFLT','results'])

""" Class for processing results from personal info queries """
class SfPersonalInfoQueryResponder(SfQueryResponder):

	def	__init__(self, dbh, kwargs):
		super().__init__(dbh, kwargs)
	
	def process(self, response):

		rec_dict = OrderedDict(
			{
				'firstName' : FlatDataItem(DataItemDataType.STRING, 'firstName', 'firstName','firstName', None),
				'middleName' : FlatDataItem(DataItemDataType.STRING, 'middleName','middleName','middleName',None),
				'lastName' : FlatDataItem(DataItemDataType.STRING, 'lastName','lastName','lastName',None),
				'suffix' : FlatDataItem(DataItemDataType.STRING, 'suffix','suffix','suffix',None),
				'preferredName' : FlatDataItem(DataItemDataType.STRING, 'preferredName','preferredName','preferredName',None),
				'displayName' : FlatDataItem(DataItemDataType.STRING, 'displayName','displayName','displayName',None),
				'gender' : FlatDataItem(DataItemDataType.STRING, 'gender','gender','gender',None),
				'salutation' : PicklistLookupItem(DataItemDataType.STRING, 'salutation', 'salutation','salutation', None),
			}
		)

		return self.getData(response, rec_dict, ['personNav','personalInfoNav','results'])

""" Class for processing results from employment info queries """
class SfEmploymentInfoQueryResponder(SfQueryResponder):

	def	__init__(self, dbh, kwargs):
		super().__init__(dbh, kwargs)
	
	def process(self, response):

		rec_dict = OrderedDict(
			{
				'hireDate' : FlatDataItem(DataItemDataType.DATE, 'startDate','hireDate','startDate',None),
				'termDate' : FlatDataItem(DataItemDataType.DATE, 'endDate','termDate','endDate',None),
				'serviceDate' : FlatDataItem(DataItemDataType.DATE, 'serviceDate','serviceDate','serviceDate',None),
				'originalStartDate' : FlatDataItem(DataItemDataType.DATE, 'originalStartDate','originalStartDate','originalStartDate',None),
				'acquisitionDate' : FlatDataItem(DataItemDataType.DATE, 'originalStartDate','acquisitionDate','originalStartDate',None),
				'lastModifiedDateTime' : FlatDataItem(DataItemDataType.DATE, 'lastModifiedDateTime','employmentLastModifiedDateTime','lastModifiedDateTime',None),
			}
		)
		
		return self.getData(response, rec_dict, None)
		
""" Class for processing results from job info queries """
class SfJobInfoQueryResponder(SfQueryResponder):

	def	__init__(self, dbh, kwargs):
		super().__init__(dbh, kwargs)
	
	def process(self, response):

		rec_dict = OrderedDict(
			{
				'employeeClass' : PicklistLookupItem(DataItemDataType.STRING, 'employeeClass','employeeClass','employeeClass',None),
				'employmentType' : PicklistLookupItem(DataItemDataType.STRING, 'employmentType', 'employmentType','employmentType',None),
				'employmentStatus' : PicklistLookupItem(DataItemDataType.STRING, 'emplStatus','employmentStatus','emplStatus',None),
				'standardHours' : FlatDataItem(DataItemDataType.STRING, 'standardHours','standardHours','standardHours',None),
				'workingDaysPerWeek' : FlatDataItem(DataItemDataType.STRING, 'workingDaysPerWeek','workingDaysPerWeek','workingDaysPerWeek',None),
				'shiftCode' : PicklistLookupItem(DataItemDataType.STRING, 'shiftCode','shiftCode','shiftCode',None),
				'bargainingUnit' : PicklistLookupItem(DataItemDataType.STRING, 'customString20','bargainingUnit','customString20',None),
				'flsaStatus' : PicklistLookupItem(DataItemDataType.STRING, 'flsaStatus','flsaStatus','flsaStatus',None),
				'eeo1JobCategory' : PicklistLookupItem(DataItemDataType.STRING, 'eeo1JobCategory','eeo1JobCategory','eeo1JobCategory',None),
				'incentivePlan' : PicklistLookupItem(DataItemDataType.STRING, 'customString22','incentivePlan','customString22',None),
				'incentivePercentage' : PicklistLookupItem(DataItemDataType.STRING, 'customString21','incentivePercentage','customString21',None),
				'jobLastModifiedDateTime' : FlatDataItem(DataItemDataType.DATE, 'lastModifiedDateTime','jobLastModifiedDateTime','lastModifiedDateTime',None),
				'positionEntryDate' : FlatDataItem(DataItemDataType.DATE, 'positionEntryDate','positionEntryDate','positionEntryDate',None),
			}
		)
		
		return self.getData(response, rec_dict, ['jobInfoNav','results'])

""" Class for processing results from location queries """
class SfLocationQueryResponder(SfQueryResponder):

	def	__init__(self, dbh, kwargs):
		super().__init__(dbh, kwargs)
	
	def process(self, response):

		rec_dict = OrderedDict(
			{
				'locationName' : FlatDataItem(DataItemDataType.STRING, 'name','locationName','name',None),
				'locationCode' : FlatDataItem(DataItemDataType.STRING, 'externalCode','locationCode','externalCode',None),
				'compRegion' : FlatDataItem(DataItemDataType.STRING, 'customString1','compRegion','customString1',None),
				'locationAAPPlan' : FlatDataItem(DataItemDataType.STRING, 'customString2','locationAAPPlan','customString2',None),
				'timezone' : FlatDataItem(DataItemDataType.STRING, 'timezone', 'locationTimezone','timezone', None),
			}
		)
		
		return self.getData(response, rec_dict, ['jobInfoNav','results','locationNav'])

""" Class for processing results from home adddress queries """
class SfLocationAddressQueryResponder(SfQueryResponder):

	def	__init__(self, dbh, kwargs):
		super().__init__(dbh, kwargs)
	
	def process(self, response):
		
		rec_dict = OrderedDict(
			{
				'address1' : FlatDataItem(DataItemDataType.STRING, 'address1','locationAddress1','address1',None),
				'zipCode' : FlatDataItem(DataItemDataType.STRING, 'zipCode','locationZipCode','zipCode',None),
				'city' : FlatDataItem(DataItemDataType.STRING, 'city','locationCity','city',None),
				'county' : FlatDataItem(DataItemDataType.STRING, 'county','locationCounty','county',None),
				'country' : FlatDataItem(DataItemDataType.STRING, 'country','locationCountry','country',None),
				'state' : FlatDataItem(DataItemDataType.STRING, 'state', 'locationState','state', None),
			}
		)

		return self.getData(response, rec_dict, ['jobInfoNav','results','locationNav','addressNavDEFLT'])

""" Class for processing results from home adddress queries """
class SfCompanyQueryResponder(SfQueryResponder):

	def	__init__(self, dbh, kwargs):
		super().__init__(dbh, kwargs)

	def process(self, response):
		
		rec_dict = OrderedDict(
			{
				'companyName' : FlatDataItem(DataItemDataType.STRING, 'name','companyName','name',None),
				'gl1Code' : FlatDataItem(DataItemDataType.STRING, 'externalCode','gl1Code','externalCode',None),
				'gl1LegalEntityType' : FlatDataItem(DataItemDataType.STRING, 'cust_legalEntityType','gl1LegalEntityType','cust_legalEntityType',None),
			}
		)

		return self.getData(response, rec_dict, ['jobInfoNav','results','companyNav'])

""" Class for processing results from home adddress queries """
class SfPositionQueryResponder(SfQueryResponder):

	def	__init__(self, dbh, kwargs):
		super().__init__(dbh, kwargs)

	def process(self, response):
		
		rec_dict = OrderedDict(
			{
				'positionTitle' : FlatDataItem(DataItemDataType.STRING, 'positionTitle','positionTitle','positionTitle',None),
				'jobTitle' : FlatDataItem(DataItemDataType.STRING, 'jobTitle','jobTitle','jobTitle',None),
				'cust_AmscoDeptCode' : FlatDataItem(DataItemDataType.STRING, 'cust_AmscoDeptCode','amscoDeptCode','cust_AmscoDeptCode',None),
				'cust_laborCode' : FlatDataItem(DataItemDataType.STRING, 'cust_laborCode','laborCode','cust_laborCode',None),
				'cust_compJobCode' : FlatDataItem(DataItemDataType.STRING, 'cust_compJobCode','compJobCode','cust_compJobCode',None),
				'cust_mpsjobdistcode' : FlatDataItem(DataItemDataType.STRING, 'cust_mpsjobdistcode','mpsJobDistCode','cust_mpsjobdistcode',None),
				'cust_CompJobTitle' : FlatDataItem(DataItemDataType.STRING, 'cust_CompJobTitle','compJobTitle','cust_CompJobTitle',None),
				'cust_carbandjoblevel' : FlatDataItem(DataItemDataType.STRING, 'cust_carbandjoblevel','carBandJobLevel','cust_carbandjoblevel',None),
				'cust_purchauthcode' : FlatDataItem(DataItemDataType.STRING, 'cust_purchauthcode','purchAuthCode','cust_purchauthcode',None),
				'cust_severancepackage' : FlatDataItem(DataItemDataType.STRING, 'cust_severancepackage','severancePackage','cust_severancepackage',None),
				'cust_LTIEligCode' : FlatDataItem(DataItemDataType.STRING, 'cust_LTIEligCode','ltiEligCode','cust_LTIEligCode',None),
				'code' : FlatDataItem(DataItemDataType.STRING, 'code','positionCode','code',None),
				'cust_KronosJob' : FlatDataItem(DataItemDataType.STRING, 'cust_KronosJob','kronosJob','cust_KronosJob',None),
				'cust_BumpEligible' : FlatDataItem(DataItemDataType.STRING, 'cust_BumpEligible','bumpEligible','cust_BumpEligible',None),
				'cust_SubBusiness' : FlatDataItem(DataItemDataType.STRING, 'cust_SubBusiness','subBusiness','cust_SubBusiness',None),
				'cust_isFulltimeEmployee' : FlatDataItem(DataItemDataType.STRING, 'cust_isFulltimeEmployee','isFulltimeEmployee','cust_isFulltimeEmployee',None),
				'cust_AAPJobGroup' : FlatDataItem(DataItemDataType.STRING, 'cust_AAPJobGroup','aapJobGroup','cust_AAPJobGroup',None),
				'cust_benExec' : FlatDataItem(DataItemDataType.STRING, 'cust_benExec','benExec','cust_benExec',None),
				'lastModifiedDateTime' : FlatDataItem(DataItemDataType.DATE, 'lastModifiedDateTime','positionLastModifiedDateTime','lastModifiedDateTime',None),
			}
		)

		return self.getData(response, rec_dict, ['jobInfoNav','results','positionNav'])

""" Class for processing results from aap group queries """
class SfAapGroupQueryResponder(SfQueryResponder):

	def	__init__(self, dbh, kwargs):
		super().__init__(dbh, kwargs)

	def process(self, response):
		
		rec_dict = OrderedDict(
			{
				'externalCode' : FlatDataItem(DataItemDataType.STRING, 'externalCode','aapGroupCode','externalCode',None),
				'externalName' : FlatDataItem(DataItemDataType.STRING, 'externalName','aapGroupName','externalName',None),
			}
		)

		return self.getData(response, rec_dict, ['jobInfoNav','results','positionNav','cust_AAPJobGroupNav'])

""" Class for processing results from pay grade queries """
class SfPayGradeQueryResponder(SfQueryResponder):

	def	__init__(self, dbh, kwargs):
		super().__init__(dbh, kwargs)

	def process(self, response):
		
		rec_dict = OrderedDict(
			{
				'externalCode' : FlatDataItem(DataItemDataType.STRING, 'externalCode','payGradeCode','externalCode',None),
				'name' : FlatDataItem(DataItemDataType.STRING, 'name','payGradeName','name',None),
				'startDate' : FlatDataItem(DataItemDataType.DATE, 'startDate','payGradeStartDate','startDate',None),
				'lastModifiedDateTime' : FlatDataItem(DataItemDataType.DATE, 'lastModifiedDateTime','payGradeLastModifiedDateTime','lastModifiedDateTime',None),
			}
		)

		return self.getData(response, rec_dict, ['jobInfoNav','results','payGradeNav'])
	
""" Class for processing results from comp queries """
class SfCompQueryResponder(SfQueryResponder):

	def	__init__(self, dbh, kwargs):
		super().__init__(dbh, kwargs)
	
	def process(self, response):

		rec_dict = OrderedDict(
			{
				'customString1' : FlatDataItem(DataItemDataType.STRING, 'customString1','payrollFileNo','customString1',None),
				'lastModifiedDateTime' : FlatDataItem(DataItemDataType.DATE, 'lastModifiedDateTime','compLastModifiedDateTime','lastModifiedDateTime',None),
			}
		)
		
		return self.getData(response, rec_dict, ['compInfoNav','results'])
		
""" Class for processing results from comp customString2 queries """
class SfComp2QueryResponder(SfQueryResponder):

	def	__init__(self, dbh, kwargs):
		super().__init__(dbh, kwargs)
	
	def process(self, response):

		rec_dict = OrderedDict(
			{
				'externalCode' : FlatDataItem(DataItemDataType.STRING, 'externalCode','payrollCompanyCode','externalCode',None),
			}
		)
		
		return self.getData(response, rec_dict, ['compInfoNav','results','customString2Nav'])

""" Class for processing results from user queries """
class SfUserQueryResponder(SfQueryResponder):

	def	__init__(self, dbh, kwargs):
		super().__init__(dbh, kwargs)
	
	def process(self, response):

		rec_dict = OrderedDict(
			{
				'username' : FlatDataItem(DataItemDataType.STRING, 'username','username','username',None),
			}
		)
		
		return self.getData(response, rec_dict, ['userNav'])

""" Class for processing results from user queries """
class SfPersonQueryResponder(SfQueryResponder):

	def	__init__(self, dbh, kwargs):
		super().__init__(dbh, kwargs)
	
	def process(self, response):

		rec_dict = OrderedDict(
			{
				'dateOfBirth' : FlatDataItem(DataItemDataType.DATE, 'dateOfBirth','dateOfBirth','dateOfBirth',None),
			}
		)
		
		return self.getData(response, rec_dict, ['personNav'])

""" Class for processing results from division queries """
class SfDivisionQueryResponder(SfQueryResponder):

	def	__init__(self, dbh, kwargs):
		super().__init__(dbh, kwargs)
	
	def process(self, response):

		rec_dict = OrderedDict(
			{
				'name' : FlatDataItem(DataItemDataType.STRING, 'name','divisionName','name',None),
				'externalCode' : FlatDataItem(DataItemDataType.STRING, 'externalCode','divisionCode','externalCode',None),
			}
		)
		
		return self.getData(response, rec_dict, ['jobInfoNav','results','divisionNav'])

""" Class for processing results from GL2 queries """
class SfGl2QueryResponder(SfQueryResponder):

	def	__init__(self, dbh, kwargs):
		super().__init__(dbh, kwargs)
	
	def process(self, response):

		rec_dict = OrderedDict(
			{
				'externalName' : FlatDataItem(DataItemDataType.STRING, 'externalName','gl2Name','externalName',None),
				'externalCode' : FlatDataItem(DataItemDataType.STRING, 'externalCode','gl2Code','externalCode',None),
			}
		)
		
		return self.getData(response, rec_dict, ['jobInfoNav','results','customString6Nav'])
		
""" Class for processing results from cost centre (GL3) queries """
class SfCostCenterQueryResponder(SfQueryResponder):

	def	__init__(self, dbh, kwargs):
		super().__init__(dbh, kwargs)
	
	def process(self, response):

		rec_dict = OrderedDict(
			{
				'name' : FlatDataItem(DataItemDataType.STRING, 'name','costCenterName','name',None),
				'externalCode' : FlatDataItem(DataItemDataType.STRING, 'externalCode','costCenterCode','externalCode',None),
			}
		)
		
		return self.getData(response, rec_dict, ['jobInfoNav','results','costCenterNav'])

""" Class for processing results from GL4 queries """
class SfGl4QueryResponder(SfQueryResponder):

	def	__init__(self, dbh, kwargs):
		super().__init__(dbh, kwargs)
	
	def process(self, response):

		rec_dict = OrderedDict(
			{
				'externalName' : FlatDataItem(DataItemDataType.STRING, 'externalName','gl4Name','externalName',None),
				'externalCode' : FlatDataItem(DataItemDataType.STRING, 'externalCode','gl4Code','externalCode',None),
			}
		)
		
		return self.getData(response, rec_dict, ['jobInfoNav','results','customString9Nav'])

""" Class for processing results from GL5 queries """
class SfGl5QueryResponder(SfQueryResponder):

	def	__init__(self, dbh, kwargs):
		super().__init__(dbh, kwargs)
	
	def process(self, response):

		rec_dict = OrderedDict(
			{
				'externalName' : FlatDataItem(DataItemDataType.STRING, 'externalName','gl5Name','externalName',None),
				'externalCode' : FlatDataItem(DataItemDataType.STRING, 'externalCode','gl5Code','externalCode',None),
			}
		)
		
		return self.getData(response, rec_dict, ['jobInfoNav','results','customString10Nav'])
		
""" Class for processing results from GL6 queries """
class SfGl6QueryResponder(SfQueryResponder):

	def	__init__(self, dbh, kwargs):
		super().__init__(dbh, kwargs)
	
	def process(self, response):

		rec_dict = OrderedDict(
			{
				'externalName' : FlatDataItem(DataItemDataType.STRING, 'externalName','gl6Name','externalName',None),
				'externalCode' : FlatDataItem(DataItemDataType.STRING, 'externalCode','gl6Code','externalCode',None),
			}
		)
		
		return self.getData(response, rec_dict, ['jobInfoNav','results','customString11Nav'])

""" Class for processing results from GL7 queries """
class SfGl7QueryResponder(SfQueryResponder):

	def	__init__(self, dbh, kwargs):
		super().__init__(dbh, kwargs)
	
	def process(self, response):

		rec_dict = OrderedDict(
			{
				'externalCode' : FlatDataItem(DataItemDataType.STRING, 'externalCode','gl7Code','externalCode',None),
			}
		)
		
		return self.getData(response, rec_dict, ['jobInfoNav','results','customString12Nav'])
		
""" Class for processing results from GL8 queries """
class SfGl8QueryResponder(SfQueryResponder):

	def	__init__(self, dbh, kwargs):
		super().__init__(dbh, kwargs)
	
	def process(self, response):

		rec_dict = OrderedDict(
			{
				'externalCode' : FlatDataItem(DataItemDataType.STRING, 'externalCode','gl8Code','externalCode',None),
			}
		)
		
		return self.getData(response, rec_dict, ['jobInfoNav','results','customString14Nav'])
		
""" Class for processing results from GL9 queries """
class SfGl9QueryResponder(SfQueryResponder):

	def	__init__(self, dbh, kwargs):
		super().__init__(dbh, kwargs)
	
	def process(self, response):

		rec_dict = OrderedDict(
			{
				'externalCode' : FlatDataItem(DataItemDataType.STRING, 'externalCode','gl9Code','externalCode',None),
			}
		)
		
		return self.getData(response, rec_dict, ['jobInfoNav','results','customString15Nav'])
		
""" Class for processing results from GL10 queries """
class SfGl10QueryResponder(SfQueryResponder):

	def	__init__(self, dbh, kwargs):
		super().__init__(dbh, kwargs)
	
	def process(self, response):

		rec_dict = OrderedDict(
			{
				'externalCode' : FlatDataItem(DataItemDataType.STRING, 'externalCode','gl10Code','externalCode',None),
			}
		)
		
		return self.getData(response, rec_dict, ['jobInfoNav','results','customString16Nav'])
	
class DbHelper:

	def __init__(self):
		
		# connect to postgresql
		# build connection string from the local_config properties
		#db connection properties
		local_config = LocalConfigParser('settings.ini')
		db_host = local_config.getValue('SfPostgres','host')
		db_database = local_config.getValue('SfPostgres','database')
		db_user = local_config.getValue('SfPostgres','user')
		db_password = local_config.getValue('SfPostgres','password')
		conn_str = 'postgresql://%(user)s:%(password)s@%(host)s/%(database)s' % {'user' : db_user, 'password' : db_password,'host' : db_host, 'database' : db_database}
		
		# return a new SqlAlchemyEngine
		self.engine = create_engine(conn_str, echo=False, pool_size=20, max_overflow=0)
		
class QueryConfig:

	def __init__(self):
		
		# placeholder for logging filehandler
		self.fh = None
		self.ch = None
		
		self.items = None

		# new db helper
		self.dbh = DbHelper()
		
	def __enter__(self):
		return self
		
	def __exit__(self, exc_type, exc_value, traceback):

		# disconnect from postgres
		# self.dbh.connection.close()
		pass
		
	def init_log(self, type, stream=True):

		log_fmt = '%(asctime)s %(name)s %(levelname)s %(message)s'
		formatter = logging.Formatter(log_fmt)
		
		logger = logging.getLogger()
		logger.setLevel(logging.DEBUG)
		
		requests_logger = logging.getLogger("requests.packages.urllib3")
		requests_logger.propagate = True
	
		if self.fh is not None:
			logger.removeHandler(self.fh)
		
		if self.ch is not None:
			logger.removeHandler(self.ch)
	
		fdm = FileDirManager()

		self.fh = logging.handlers.RotatingFileHandler(os.path.join(fdm.get_log_dir(),type + '.log'),'a',1000000,5)
		self.fh.setFormatter(formatter)
		self.fh.setLevel(logging.DEBUG)
		logger.addHandler(self.fh)
		
		if stream:
			self.ch = logging.StreamHandler()
			self.ch.setFormatter(formatter)
			self.ch.setLevel(logging.DEBUG)
			logger.addHandler(self.ch)
	
	def get_item(self, type):
	
		for i in self.items:
			if i['type'] == type:
				return i
				
	def get_all_items(self):
		return self.items
	
class ECQueryConfig(QueryConfig):

	def __init__(self):
		
		# init parent
		super().__init__()
		
		# queries
		self.items = [
			{
				'type' : 'home_address', 
				'enc_csv_name' : 'home_address.csv',
				'odata_base_entity' : 'EmpEmployment',
				'odata_query_params' : {'$expand' : 'personNav/homeAddressNavDEFLT','$select' : 'personNav/homeAddressNavDEFLT/address1,personNav/homeAddressNavDEFLT/address2,personNav/homeAddressNavDEFLT/address3,personNav/homeAddressNavDEFLT/city,personNav/homeAddressNavDEFLT/county,personNav/homeAddressNavDEFLT/country,personNav/homeAddressNavDEFLT/startDate,personNav/homeAddressNavDEFLT/endDate,personNav/homeAddressNavDEFLT/state'},
				'add_key' : {'api' : 'personIdExternal', 'df' : 'personIdExternal'},
				'paging' : 'cursor',
				'priority_weight' : 2
			},
			{
				'type' : 'phone',
				'enc_csv_name' : 'phone.csv',
				'odata_base_entity' : 'EmpEmployment',
				'odata_query_params' : {'$expand' : 'personNav/phoneNav',
										'$select' : 'personNav/phoneNav/phoneNumber,personNav/phoneNav/phoneType'},
				'add_key' : {'api' : 'personIdExternal', 'df' : 'personIdExternal'},
				'paging' : 'cursor',
				'priority_weight' : 2
			},
			{
				'type' : 'location',
				'enc_csv_name' : 'location.csv',
				'odata_base_entity' : 'EmpEmployment',
				'odata_query_params' : {
					'$expand' : 'jobInfoNav/locationNav',
					'$select' : 'jobInfoNav/locationNav/name,jobInfoNav/locationNav/externalCode,jobInfoNav/locationNav/customString1,jobInfoNav/locationNav/customString2,jobInfoNav/locationNav/timezone',},
				'add_key' : {'api' : 'personIdExternal', 'df' : 'personIdExternal'},
				'paging' : 'cursor',
				'priority_weight' : 2
			},
			{
				'type' : 'location_address', 
				'enc_csv_name' : 'location_address.csv',
				'odata_base_entity' : 'EmpEmployment',
				'odata_query_params' : {'$expand' : 'jobInfoNav/locationNav/addressNavDEFLT',
						'$select' : 'jobInfoNav/locationNav/addressNavDEFLT/address1,jobInfoNav/locationNav/addressNavDEFLT/zipCode,jobInfoNav/locationNav/addressNavDEFLT/city,jobInfoNav/locationNav/addressNavDEFLT/county,jobInfoNav/locationNav/addressNavDEFLT/country,jobInfoNav/locationNav/addressNavDEFLT/state'},
				'add_key' : {'api' : 'personIdExternal', 'df' : 'personIdExternal'},
				'paging' : 'cursor',
				'priority_weight' : 2
			},
			{
				'type' : 'company', 
				'enc_csv_name' : 'company.csv',
				'odata_base_entity' : 'EmpEmployment',
				'odata_query_params' : {
					'$expand' : 'jobInfoNav/companyNav',
					'$select' : 'jobInfoNav/companyNav/name,jobInfoNav/companyNav/externalCode,jobInfoNav/companyNav/cust_legalEntityType'},
				'add_key' : {'api' : 'personIdExternal', 'df' : 'personIdExternal'},
				'paging' : 'cursor',
				'priority_weight' : 2
			},
			{
				'type' : 'email',
				'enc_csv_name' : 'email.csv',
				'odata_base_entity' : 'EmpEmployment',
				'odata_query_params' : {'$expand' : 'personNav/emailNav',
										'$select' : 'personNav/emailNav/emailAddress,personNav/emailNav/emailType'},
				'add_key' : {'api' : 'personIdExternal', 'df' : 'personIdExternal'},
				'paging' : 'cursor',
				'priority_weight' : 2
			},
			{
				'type' : 'personal_info',
				'enc_csv_name' : 'personal_info.csv',
				'odata_base_entity' : 'EmpEmployment',
				'odata_query_params' : {'$expand' : 'personNav/personalInfoNav',
				'$select' : 'personNav/personalInfoNav/firstName,personNav/personalInfoNav/middleName,personNav/personalInfoNav/lastName,personNav/personalInfoNav/suffix,personNav/personalInfoNav/preferredName,personNav/personalInfoNav/displayName,personNav/personalInfoNav/gender,personNav/personalInfoNav/salutation'},
				'add_key' : {'api' : 'personIdExternal', 'df' : 'personIdExternal'},
				'paging' : 'cursor',
				'priority_weight' : 10
			},
			{
				'type' : 'job_info',
				'enc_csv_name' : 'job_info.csv',
				'odata_base_entity' : 'EmpEmployment',
				'odata_query_params' : {
					'$expand' : 'jobInfoNav',
					'$select' : 'jobInfoNav/jobTitle,jobInfoNav/employeeClass,jobInfoNav/employmentType,jobInfoNav/emplStatus,jobInfoNav/standardHours,jobInfoNav/workingDaysPerWeek,jobInfoNav/shiftCode,jobInfoNav/customString20,jobInfoNav/flsaStatus,jobInfoNav/eeo1JobCategory,jobInfoNav/customString22,jobInfoNav/customString21,jobInfoNav/lastModifiedDateTime,jobInfoNav/positionEntryDate'},
				'add_key' : {'api' : 'personIdExternal', 'df' : 'personIdExternal'},
				'paging' : 'cursor',
				'priority_weight' : 10
			},
			{
				'type' : 'emp_info',
				'enc_csv_name' : 'employment_info.csv',
				'odata_base_entity' : 'EmpEmployment',
				'odata_query_params' : {
					'$select' : 'startDate,endDate,serviceDate,originalStartDate,customDate5,lastModifiedDateTime'
				},
				'add_key' : {'api' : 'personIdExternal', 'df' : 'personIdExternal'},
				'paging' : 'cursor',
				'priority_weight' : 2
			},
			{
				'type' : 'position',
				'enc_csv_name' : 'position.csv',
				'odata_base_entity' : 'EmpEmployment',
				'odata_query_params' : {
					'$expand' : 'jobInfoNav/positionNav',
					'$select' : 'jobInfoNav/positionNav/positionTitle, jobInfoNav/positionNav/jobTitle, jobInfoNav/positionNav/cust_AmscoDeptCode, jobInfoNav/positionNav/cust_laborCode, jobInfoNav/positionNav/cust_compJobCode, jobInfoNav/positionNav/cust_mpsjobdistcode, jobInfoNav/positionNav/cust_CompJobTitle, jobInfoNav/positionNav/cust_carbandjoblevel, jobInfoNav/positionNav/cust_purchauthcode, jobInfoNav/positionNav/cust_severancepackage, jobInfoNav/positionNav/cust_LTIEligCode, jobInfoNav/positionNav/lastModifiedDateTime, jobInfoNav/positionNav/code, jobInfoNav/positionNav/cust_KronosJob, jobInfoNav/positionNav/cust_BumpEligible, jobInfoNav/positionNav/cust_SubBusiness, jobInfoNav/positionNav/cust_isFulltimeEmployee, jobInfoNav/positionNav/cust_AAPJobGroup, jobInfoNav/positionNav/cust_benExec'
				},
				'add_key' : {'api' : 'personIdExternal', 'df' : 'personIdExternal'},
				'paging' : 'cursor',
				'priority_weight' : 10
			},
			{
				'type' : 'comp',
				'enc_csv_name' : 'comp.csv',
				'odata_base_entity' : 'EmpEmployment',
				'odata_query_params' : {
					'$expand' : 'compInfoNav',
					'$select' : 'compInfoNav/customString1,compInfoNav/lastModifiedDateTime'
				},
				'add_key' : {'api' : 'personIdExternal', 'df' : 'personIdExternal'},
				'paging' : 'cursor',
				'priority_weight' : 2
			},
			{
				'type' : 'comp_two',
				'enc_csv_name' : 'comp_two.csv',
				'odata_base_entity' : 'EmpEmployment',
				'odata_query_params' : {
					'$expand' : 'compInfoNav/customString2Nav',
					'$select' : 'compInfoNav/customString2Nav/externalCode'
				},
				'add_key' : {'api' : 'personIdExternal', 'df' : 'personIdExternal'},
				'paging' : 'cursor',
				'priority_weight' : 2
			},
			{
				'type' : 'aap_group',
				'enc_csv_name' : 'aap_group.csv',
				'odata_base_entity' : 'EmpEmployment',
				'odata_query_params' : {
					'$expand' : 'jobInfoNav/positionNav/cust_AAPJobGroupNav',
					'$select' : 'jobInfoNav/positionNav/cust_AAPJobGroupNav/externalCode,jobInfoNav/positionNav/cust_AAPJobGroupNav/externalName'
				},
				'add_key' : {'api' : 'personIdExternal', 'df' : 'personIdExternal'},
				'paging' : 'cursor',
				'priority_weight' : 2
			},
			{
				'type' : 'pay_grade',
				'enc_csv_name' : 'pay_grade.csv',
				'odata_base_entity' : 'EmpEmployment',
				'odata_query_params' : {
					'$expand' : 'jobInfoNav/payGradeNav',
					'$select' : 'jobInfoNav/payGradeNav/externalCode,jobInfoNav/payGradeNav/name,jobInfoNav/payGradeNav/startDate,jobInfoNav/payGradeNav/lastModifiedDateTime'
				},
				'add_key' : {'api' : 'personIdExternal', 'df' : 'personIdExternal'},
				'paging' : 'cursor',
				'priority_weight' : 2
			},
			{
				'type' : 'user',
				'enc_csv_name' : 'user.csv',
				'odata_base_entity' : 'EmpEmployment',
				'odata_query_params' : {
					'$expand' : 'userNav',
					'$select' : 'userNav/username'
				},
				'add_key' : {'api' : 'personIdExternal', 'df' : 'personIdExternal'},
				'paging' : 'cursor',
				'priority_weight' : 2
			},
			{
				'type' : 'person',
				'enc_csv_name' : 'person.csv',
				'odata_base_entity' : 'EmpEmployment',
				'odata_query_params' : {
					'$expand' : 'personNav',
					'$select' : 'personNav/dateOfBirth'
				},
				'add_key' : {'api' : 'personIdExternal', 'df' : 'personIdExternal'},
				'paging' : 'cursor',
				'priority_weight' : 2
			},
			{
				'type' : 'division',
				'enc_csv_name' : 'division.csv',
				'odata_base_entity' : 'EmpEmployment',
				'odata_query_params' : {
					'$expand' : 'jobInfoNav/divisionNav',
					'$select' : 'jobInfoNav/divisionNav/name,jobInfoNav/divisionNav/externalCode'
				},
				'add_key' : {'api' : 'personIdExternal', 'df' : 'personIdExternal'},
				'paging' : 'cursor',
				'priority_weight' : 2
			},
			{
				'type' : 'gl2',
				'enc_csv_name' : 'gl2.csv',
				'odata_base_entity' : 'EmpEmployment',
				'odata_query_params' : {
					'$expand' : 'jobInfoNav/customString6Nav',
					'$select' : 'jobInfoNav/customString6Nav/externalName,jobInfoNav/customString6Nav/externalCode'
				},
				'add_key' : {'api' : 'personIdExternal', 'df' : 'personIdExternal'},
				'paging' : 'cursor',
				'priority_weight' : 2
			},
			{
				'type' : 'cost_center',
				'enc_csv_name' : 'cost_center.csv',
				'odata_base_entity' : 'EmpEmployment',
				'odata_query_params' : {
					'$expand' : 'jobInfoNav/costCenterNav',
					'$select' : 'jobInfoNav/costCenterNav/name,jobInfoNav/costCenterNav/externalCode'
				},
				'add_key' : {'api' : 'personIdExternal', 'df' : 'personIdExternal'},
				'paging' : 'cursor',
				'priority_weight' : 2
			},
			{
				'type' : 'gl4',
				'enc_csv_name' : 'gl4.csv',
				'odata_base_entity' : 'EmpEmployment',
				'odata_query_params' : {
					'$expand' : 'jobInfoNav/customString9Nav',
					'$select' : 'jobInfoNav/customString9Nav/externalName,jobInfoNav/customString9Nav/externalCode'
				},
				'add_key' : {'api' : 'personIdExternal', 'df' : 'personIdExternal'},
				'paging' : 'cursor',
				'priority_weight' : 2
			},
			{
				'type' : 'gl5',
				'enc_csv_name' : 'gl5.csv',
				'odata_base_entity' : 'EmpEmployment',
				'odata_query_params' : {
					'$expand' : 'jobInfoNav/customString10Nav',
					'$select' : 'jobInfoNav/customString10Nav/externalName,jobInfoNav/customString10Nav/externalCode'
				},
				'add_key' : {'api' : 'personIdExternal', 'df' : 'personIdExternal'},
				'paging' : 'cursor',
				'priority_weight' : 2
			},
			{
				'type' : 'gl6',
				'enc_csv_name' : 'gl6.csv',
				'odata_base_entity' : 'EmpEmployment',
				'odata_query_params' : {
					'$expand' : 'jobInfoNav/customString11Nav',
					'$select' : 'jobInfoNav/customString11Nav/externalName,jobInfoNav/customString11Nav/externalCode'
				},
				'add_key' : {'api' : 'personIdExternal', 'df' : 'personIdExternal'},
				'paging' : 'cursor',
				'priority_weight' : 2
			},
			{
				'type' : 'gl7',
				'enc_csv_name' : 'gl7.csv',
				'odata_base_entity' : 'EmpEmployment',
				'odata_query_params' : {
					'$expand' : 'jobInfoNav/customString12Nav',
					'$select' : 'jobInfoNav/customString12Nav/externalCode'
				},
				'add_key' : {'api' : 'personIdExternal', 'df' : 'personIdExternal'},
				'paging' : 'cursor',
				'priority_weight' : 2
			},
			{
				'type' : 'gl8',
				'enc_csv_name' : 'gl8.csv',
				'odata_base_entity' : 'EmpEmployment',
				'odata_query_params' : {
					'$expand' : 'jobInfoNav/customString14Nav',
					'$select' : 'jobInfoNav/customString14Nav/externalCode'
				},
				'add_key' : {'api' : 'personIdExternal', 'df' : 'personIdExternal'},
				'paging' : 'cursor',
				'priority_weight' : 2
			},
			{
				'type' : 'gl9',
				'enc_csv_name' : 'gl9.csv',
				'odata_base_entity' : 'EmpEmployment',
				'odata_query_params' : {
					'$expand' : 'jobInfoNav/customString15Nav',
					'$select' : 'jobInfoNav/customString15Nav/externalCode'
				},
				'add_key' : {'api' : 'personIdExternal', 'df' : 'personIdExternal'},
				'paging' : 'cursor',
				'priority_weight' : 2
			},
			{
				'type' : 'gl10',
				'enc_csv_name' : 'gl10.csv',
				'odata_base_entity' : 'EmpEmployment',
				'odata_query_params' : {
					'$expand' : 'jobInfoNav/customString16Nav',
					'$select' : 'jobInfoNav/customString16Nav/externalCode'
				},
				'add_key' : {'api' : 'personIdExternal', 'df' : 'personIdExternal'},
				'paging' : 'cursor',
				'priority_weight' : 2
			},
			
		]	

if __name__ == "__main__":

	with ECQueryConfig() as qc:
	
		#for item in qc.get_all():
		item = qc.get_item('phone')
		f = QueryRunnerFactory()
		q = f.make(qc, item)
		q.run()
