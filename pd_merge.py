import os, collections
from sqlalchemy import create_engine
import pandas as pd
import fnmatch, re, sys
from io import StringIO
from csv_encryptor import CsvEncryptor
from local_config import LocalConfigParser
from file_dir_manager import FileDirManager
from sftp_operation import SftpOperation

class CsvMerger:

	def __init__(self):
		self.key_column = 'personIdExternal'
		self.local_config = LocalConfigParser('settings.ini')

	def run(self):
	
		compoundDf = None
		fdm = FileDirManager()
		enc = CsvEncryptor()
		enc_dir = fdm.get_enc_dir()

		# filter for the walk method
		includes = ['*.enc'] # for files only

		# transform glob patterns to regular expressions
		includes = r'|'.join([fnmatch.translate(x) for x in includes])

		for root, dirs, files in os.walk(enc_dir):

			# exclude/include files
			files = [os.path.join(root, f) for f in files]
			files = [f for f in files if re.match(includes, f)]

			for fname in sorted(files):
				
				dCsv = StringIO(enc.decrypt(fname))
				df = pd.read_csv(dCsv, dtype=str)
				if compoundDf is None:
					compoundDf = df
				else:
					compoundDf = compoundDf.merge(df,left_on=(self.key_column,),right_on=(self.key_column,), how='inner', copy=False, left_index = True)
		
		"""
		# new sqlalchemy engine
		engine = create_engine('mysql+pymysql://airflow:zRrS4U8iTWqJ9mIzjWZB@localhost/sfapi_odata?charset=utf8mb4', echo=False)
		
		# data frame to sql using the sqlalchemy engine
		compoundDf.to_sql('compound',engine,if_exists='replace',index=False)
		"""
		
		"""
		# Demonstrates how to reorder the columns in a dataframe
		cols = ['personIdExternal','emailAddress','emailType','firstName','lastName','middleName','lastModifiedDateTime','address1','address2']
		compoundDf = compoundDf[cols]
		"""

		enc_res_file_name = os.path.join(enc_dir,self.local_config.getValue('Merge','resultfilename'))
		result_io = StringIO()
		compoundDf.to_csv(result_io, index=False)
		compoundDf.to_csv(os.path.join(fdm.get_result_dir(),'result.csv'), index=False)
		enc.encrypt(result_io, enc_res_file_name)

if __name__ == "__main__":
	cm = CsvMerger()
	cm.run()