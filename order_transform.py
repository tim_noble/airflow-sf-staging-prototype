import csv, os, json
import pandas as pd
from file_dir_manager import FileDirManager
from pprint import pprint
from collections import OrderedDict
from csv_encryptor import CsvEncryptor
from io import StringIO

class OrderTransformer:

	def run(self):

		fdm = FileDirManager()
		#df = pd.read_csv(os.path.join(fdm.get_result_dir(),'result.csv'),dtype=object)

		with open(os.path.join(os.path.dirname(os.path.realpath(__file__)),'order_transform_map.json')) as f:
			map = json.load(f)

		transformed_result = []
		
		enc_res_file_name = os.path.join(fdm.get_enc_dir(),'result.enc')

		csv_enc = CsvEncryptor()

		sio = StringIO(csv_enc.decrypt(enc_res_file_name))

		reader = csv.DictReader(sio)

		for row in reader:
			transformed = OrderedDict()
			for m in map:
				s = m['source']
				t = m['target']
				v = None
				if s in row:
					vs = row[s]
					v = vs if len(vs.strip()) > 0 else None
				transformed[t] = v
			transformed_result.append(transformed)
		
		# remove the sio from memory
		sio = None
	
		# write the transformed CSV to a result StringIO		
		result_io = StringIO()
		writer = csv.DictWriter(result_io, fieldnames=transformed_result[0].keys())
		writer.writeheader()
		writer.writerows(transformed_result)
		
		# encrypt result again
		csv_enc.encrypt(result_io, enc_res_file_name)
		
		with open(os.path.join(fdm.get_result_dir(),'kk_result.csv'), 'w') as f:
		
			writer = csv.DictWriter(f, fieldnames=transformed_result[0].keys())
			writer.writeheader()
			writer.writerows(transformed_result)

		# write to csv temporarily
		#pd.to_csv(os.path.join(fdm.get_result_dir(),'kk_result.csv'), transformed_result)	
			
if __name__ == "__main__":
	cm = OrderTransformer()
	cm.run()