from core import SfOdataClient, SfQuery, SfQueryRunner, SfQueryResponder, FlatDataItem, DataItemDataType
from collections import OrderedDict
from core import QueryConfig

# client = SfOdataClient('picklist')

""" Returns query runner classes in response to config params """		
class QueryRunnerFactory:

	def make(self, qc, kwargs):
	
		qc.init_log(kwargs['type'])
		
		return SfQueryRunner(SfQuery(kwargs),SfPicklistQueryResponder(qc.dbh, kwargs))

	"""
	def make(self, qc, kwargs):
		
		if (kwargs['type'] == 'picklist_labels'):
			return SfQueryRunner(log_utils, SfQuery(kwargs),SfPicklistQueryResponder(kwargs))
	"""

""" Class for processing results from employment info queries """
class SfPicklistQueryResponder(SfQueryResponder):

	def	__init__(self, dbh, kwargs):
		super().__init__(dbh, kwargs)
	
	def process(self, response):

		rec_dict = OrderedDict(
			{
				'optionId' : FlatDataItem(DataItemDataType.STRING, 'optionId','option_id','optionId',None),
				'label' : FlatDataItem(DataItemDataType.STRING, 'label','label','label',None),
				'locale' : FlatDataItem(DataItemDataType.STRING, 'locale','locale','locale',None),
			}
		)
		
		return self.getData(response, rec_dict, None)

class PLQueryConfig(QueryConfig):

	def get(self):
		
		return [
			{
				'type' : 'picklist_labels',
				'enc_csv_name' : 'picklist_labels.csv',
				'odata_base_entity' : 'PicklistLabel',
				'odata_query_params' : {
					'$filter' : 'locale eq \'en_US\'',
					'$select' : 'optionId,label,locale'},
				'add_key' : {'api' : 'optionId', 'df' : 'option_id'}
			},
		]
		
if __name__ == "__main__":

	logging.getLogger().setLevel(logging.INFO)
	
	with PLQueryConfig() as qc:
	
		for item in qc.get():
			f = QueryRunnerFactory()
			q = f.make(qc, item)
			q.run()