import os
from local_config import LocalConfigParser

class FileDirManager:

	def __init__(self):
		
		self.local_config = LocalConfigParser(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'settings.ini'))
		self.base_dir = os.path.dirname(os.path.realpath(__file__))
		
	def get_files_dir(self,):
	
		return os.path.join(self.base_dir,'files')
		
	def get_full_path(self, relative_path):
	
		return os.path.join(self.get_files_dir(),relative_path)
		
	def get_result_dir(self):
	
		return self.get_full_path(self.local_config.getValue('Dir','result'))
		
	def get_enc_dir(self):
	
		return self.get_full_path(self.local_config.getValue('Dir','enc'))
		
	def get_log_dir(self):
	
		return self.get_full_path(self.local_config.getValue('Dir','log'))
		
	def get_error_dir(self):
	
		return self.get_full_path(self.local_config.getValue('Dir','error'))