import sys, os
# inserts the current directory to the Python path allowing us to reference code stored in it
sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)))
import airflow
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators.email_operator import EmailOperator
from datetime import datetime, timedelta
import json
from pprint import pprint
import time
import smtplib
import logging
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from local_config import LocalConfigParser
from core import ECQueryConfig as OdQueryConfig
from core import QueryRunnerFactory as OdQueryRunnerFactory
from pd_merge import CsvMerger
from picklist_get import PLQueryConfig
from picklist_get import QueryRunnerFactory as PLQueryRunnerFactory
from picklist_import import ImportProcess as PiImportProcess
from file_dir_manager import FileDirManager
from gpg_encrypt import GpgEncryptor
from gpg_move import GpgMover
from order_transform import OrderTransformer
from tidy_files import FileTidier
from result_get import ResultGetter

def tidy_file_dirs(ds, **kwargs):
	
	t = FileTidier()
	t.run()

def result_get(ds, **kwargs):
	
	r = ResultGetter()
	r.run()

def picklist_get(ds, **kwargs):

	pqc = kwargs['query_config']
	
	for item in pqc.get():
		f = PLQueryRunnerFactory()
		q = f.make(pqc, item)
		q.run()
	
def picklist_import(ds, **kwargs):
	qi = PiImportProcess()
	qi.run()
	
def gpg_encrypt(ds, **kwargs):
	qi = GpgEncryptor()
	qi.run()

def gpg_move(ds, **kwargs):
	qi = GpgMover()
	qi.run()
	
def query_odata(ds, **kwargs):

	item = kwargs['item']
	query_config = kwargs['query_config']
	f = OdQueryRunnerFactory()
	q = f.make(query_config, item)
	q.run()
	
def pd_merge(ds, **kwargs):
	"""
		Merges encrypted CSVs to compound csv
	"""
	#raise RuntimeError("This is a test")
	cm = CsvMerger()
	cm.run()
	
def order_transform_merge(ds, **kwargs):
	cm = OrderTransformer()
	cm.run()
	
"""
Main body
"""
# get local ini
local_config = LocalConfigParser('settings.ini')

# dag properties from ini
start_date = datetime.strptime(local_config.getValue('Dag','startdate'), local_config.getValue('Dag','startdateformat'))
retries = int(local_config.getValue('Dag','retries'))
schedule_interval_secs = int(local_config.getValue('Dag','scheduleintervalseconds'))
retry_delay_secs = int(local_config.getValue('Dag','retrydelayseconds'))

logging.getLogger().setLevel(logging.INFO)

# these args will get passed on to each operator
# you can override them on a per-task basis during operator initialization
default_args = {
	'owner': 'tim_noble',
	'depends_on_past': False,
	'start_date': start_date,
	'email': ['airflow@tim-noble.co.uk'],
	'email_on_failure': True,
	'email_on_retry': True,
	'retries': retries,
	'max_active_runs' : 1,
	'retry_delay': timedelta(seconds=retry_delay_secs),
	'concurrency' : 1,
	'catchup' : False,
	'execution_timeout': timedelta(hours=1),
	# 'queue': 'bash_queue',
	# 'pool': 'backfill',
	# 'priority_weight': 10,
	# 'end_date': datetime(2016, 1, 1),
	# 'wait_for_downstream': False,
	# 'dag': dag,
	# 'adhoc':False,
	# 'sla': timedelta(hours=2),
	# 'on_failure_callback': some_function,
	# 'on_success_callback': some_other_function,
	# 'on_retry_callback': another_function,
	# 'trigger_rule': u'all_success'
}

# the timedelta in schedule_interval must be less than the timedelta in start_date in default_args
dag = DAG(
	'sf_staging',
	default_args=default_args,
	description='Parallel download of full OData files from SuccessFactors',
	schedule_interval=timedelta(seconds=schedule_interval_secs))

dag.doc_md = __doc__

pqc = PLQueryConfig()

t_tfd = PythonOperator(
	task_id='tidy_file_dirs',
	python_callable=tidy_file_dirs,
	op_kwargs={'local_config' : local_config},
	provide_context=True,
	dag=dag)

"""
t_plg = PythonOperator(
	task_id='picklist_get',
	python_callable=picklist_get,
	provide_context=True,
	op_kwargs={'query_config' : pqc},
	dag=dag)
	
t_plg.set_upstream(t_tfd)
	
t_pli = PythonOperator(
	task_id='picklist_import',
	python_callable=picklist_import,
	provide_context=True,
	dag=dag)
	
t_pli.set_upstream(t_plg)
"""

# get download files from sftp
t_rget = PythonOperator(
	task_id='result_get',
	python_callable=result_get,
	provide_context=True,
	dag=dag)

# merge downloads
t_merge = PythonOperator(
	task_id='result_merge',
	python_callable=pd_merge,
	provide_context=True,
	dag=dag)
	
t_merge.set_upstream(t_rget)
	
# >>>>>>>>>> Set up parallel queries here
oqc = OdQueryConfig()

for item in oqc.get_all_items():
	type = item['type']
	t = PythonOperator(
		task_id=type,
		python_callable=query_odata,
		op_kwargs={'query_config' : oqc, 'item': item},
		priority_weight=item['priority_weight'],
		provide_context=True,
		dag=dag)
	#t.set_upstream(t_pli)
	t.set_upstream(t_tfd)
	t.set_downstream(t_rget)
# >>>>>>>>>> End parallel queries here

t_ort = PythonOperator(
	task_id='order_transform_merge',
	python_callable=order_transform_merge,
	provide_context=True,
	dag=dag)
	
t_ort.set_upstream(t_merge)

t_enc = PythonOperator(
	task_id='gpg_encrypt_ordered',
	python_callable=gpg_encrypt,
	provide_context=True,
	dag=dag)
	
t_enc.set_upstream(t_ort)
	
t_mov = PythonOperator(
	task_id='gpg_move',
	python_callable=gpg_move,
	provide_context=True,
	dag=dag)
	
t_mov.set_upstream(t_enc)


