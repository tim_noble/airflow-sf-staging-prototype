import os, gnupg, logging
from local_config import LocalConfigParser
from file_dir_manager import FileDirManager
from io import StringIO
from csv_encryptor import CsvEncryptor

class GpgEncryptor:

	def __init__(self):

		self.local_config = LocalConfigParser('settings.ini')
		fdm = FileDirManager()
		self.enc_res_file_name = os.path.join(fdm.get_enc_dir(),self.local_config.getValue('Merge','resultfilename'))
		self.gpg_target_path = os.path.join(fdm.get_result_dir(),self.local_config.getValue('Gpg','resultfilename'))

	def run(self):
		
		# location of gnugpghome from ini
		gnugpghome = self.local_config.getValue('Gpg','gnugpghome')

		# gpg using keys in current user's home
		gpg = gnupg.GPG(gnupghome=gnugpghome)
		
		# recipient for whom error messages should be encrypted
		recipient = self.local_config.getValue('Gpg','resultrecipient')
		
		# decrypt sodium encrypted string in RAM
		enc = CsvEncryptor()
		sio = StringIO(enc.decrypt(self.enc_res_file_name))
		
		# convert to utf-8 in RAM, ignoring any invalid characters
		dec_str = sio.getvalue().encode('utf-8', 'ignore')

		# re-encrypt string for GPG and write to disk
		with open (self.gpg_target_path, 'w') as f:
			f.write(str(gpg.encrypt(dec_str, recipient)))

if __name__ == "__main__":
	cm = GpgEncryptor()
	cm.run()