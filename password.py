import pysodium, os, base64

"""
key = os.urandom(pysodium.crypto_box_SECRETKEYBYTES)
print(base64.b64encode(key))
nonce = os.urandom(pysodium.crypto_box_NONCEBYTES)
print(base64.b64encode(nonce))
"""

"""
One way of storing passwords encrypted and decrypting them on startup
"""

"""
Generate a key
"""
with open('key', 'wb') as k:
	key = os.urandom(pysodium.crypto_box_SECRETKEYBYTES)
	k.write(base64.b64encode(key))

"""
Generate a nonce
"""
with open('nonce', 'wb') as n:
	nonce = os.urandom(pysodium.crypto_box_NONCEBYTES)
	n.write(base64.b64encode(nonce))

"""
Encrypt a message
"""
with open('key', 'rb') as k:

	key = base64.b64decode(k.read())
	
	with open('nonce', 'rb') as n:
	
		nonce = base64.b64decode(n.read())

		msg = 'apiuser@steriscorpT1'.encode('utf-8')

		with open('message', 'wb') as m:
			m.write(base64.b64encode(pysodium.crypto_secretbox(msg, nonce, key)))

"""
Decrypt a message
"""
with open('key', 'rb') as k:

	key = base64.b64decode(k.read())
	
	with open('nonce', 'rb') as n:
	
		nonce = base64.b64decode(n.read())
		
		with open('message','rb') as t:
			o = pysodium.crypto_secretbox_open(base64.b64decode(t.read()), nonce, key);
			print(o.decode('utf-8'))